// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "TDS/Interface/TDS_IGameActor.h"
#include "DrawDebugHelpers.h"

#include "Net/UnrealNetwork.h"

int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVAREExplodeShow(
	TEXT("TDS.DebugExplode"),
	DebugExplodeShow,
	TEXT("Draw Debug for Explode"), ECVF_Cheat
);

void AProjectileDefault_Grenade::BeginPlay()
{

	Super::BeginPlay();
	

}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);


}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
			Explose();

		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!TimerEnabled)
	{
		Explose();
	}
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose()
{
	SetReplicates(true);

	if (DebugExplodeShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Green, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Red, false, 12.0f);
	}

	TimerEnabled = false;
	
	FxGrenadeExplose_Multicast(ProjectileSetting.ExploseFX, ProjectileSetting.ExploseSound);
	APawn* FiringPawn = GetInstigator();
	if (FiringPawn && HasAuthority())
	{
		AController* FiringController = FiringPawn->GetController();
		if (FiringController) {
			TArray<AActor*> IgnoredActor;
			UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
				ProjectileSetting.ExploseMaxDamage,
				ProjectileSetting.ExploseMaxDamage * 0.2f,
				GetActorLocation(),
				ProjectileSetting.ProjectileMinRadiusDamage,
				ProjectileSetting.ProjectileMaxRadiusDamage,
				5,
				NULL, IgnoredActor, nullptr, nullptr);
				

			this->Destroy();
		}
	}
}

void AProjectileDefault_Grenade::FxGrenadeExplose_Multicast_Implementation(UParticleSystem* FxExplose, USoundBase* SoundExplose)
{
	Super::Destroyed();

	if (FxExplose)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxExplose, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (SoundExplose)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), SoundExplose, GetActorLocation());
	}
}
